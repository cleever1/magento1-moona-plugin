<?php

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

$data = $installer->getConnection()->fetchAll("SHOW COLUMNS FROM `{$installer->getTable('sales_flat_order')}`;");

$columnSession = false;
$columnMode = false;

foreach ($data as $row) {
    if ($row['Field'] == 'session_id') {
        $columnSession = true;
        break;
    }

    if ($row['Field'] == 'moona_mode') {
        $columnMode = true;
        break;
    }
}

if (!$columnSession) {
    $installer->run("
        ALTER TABLE `{$installer->getTable('sales_flat_order')}`
        ADD COLUMN `session_id` VARCHAR(255) NULL COMMENT 'session_id';
    ");
}

if (!$columnMode) {
    $installer->run("
        ALTER TABLE `{$installer->getTable('sales_flat_order')}`
        ADD COLUMN `moona_mode` VARCHAR(255) NULL COMMENT 'moona_mode';
    ");
}

$installer->endSetup();
