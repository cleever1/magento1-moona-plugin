<?php

class Moona_Moona_Block_Form extends Mage_Payment_Block_Form
{

    /**
     * @return Moona_Moona_Model_Banner
     */
    public function getBannerInstance()
    {
        return Mage::getSingleton('moona/banner');
    }

    /**
     * @return Moona_Moona_Model_Api
     */
    public function getApiInstance()
    {
        return Mage::getSingleton('moona/api');
    }

    protected function _construct()
    {
        $this->setTemplate('moona/form.phtml');
        parent::_construct();
    }

    public function getDataBanners()
    {
        $orderIncrementId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
        $order = Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);

        $data = $this->getApiInstance()->getCheckoutBanner(array(
            'price' => ($order->getBaseGrandTotal()*100),
            'lang' => Mage::app()->getLocale()->getLocaleCode()
        ));

        if($data['error']) {
            Mage::throwException('An error has occured. '. $data['response'] );
        }

        $response = json_decode($data['response'],true);
        $response['banners']['enabled'] = $this->getBannerInstance()->isBannerAvailable('banner_above_cta_checkout_page');
        $response['icon'] = isset($response['icon']) ? $response['icon'] : false;

        return $response;
    }

}