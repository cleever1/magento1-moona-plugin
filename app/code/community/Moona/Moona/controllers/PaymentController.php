<?php

class Moona_Moona_PaymentController extends Mage_Core_Controller_Front_Action
{
    /**
     *
     * @return Moona_Moona_Model_Payment
     */
    public function getMethodInstance()
    {
        return Mage::getSingleton('moona/payment');
    }

    /**
     *
     * @return Moona_Moona_Model_Api
     */
    public function getApiInstance()
    {
        return Mage::getSingleton('moona/api');
    }

    public function redirectAction()
    {
        $data = $this->getApiInstance()->getDisplayMode();
        $response = json_decode($data['response'], true);

        $mode = $data['error'] ? 'redirection' : $response['mode'];

        if($mode == 'iframe') {
            $this->loadLayout();
        }

        $session = Mage::getSingleton('checkout/session');
        $session->setMoonaQuoteId($session->getQuoteId());
        $this->getResponse()->setBody(
            $mode == 'iframe' ?
                $this->_getIframeBlock()->toHtml() :
                $this->getLayout()->createBlock('moona/redirect')->toHtml()
        );
        $session->unsQuoteId();
        $session->unsRedirectUrl();

        if($mode == 'iframe') {
            $this->renderLayout();
        }
    }

    public function backAction()
    {
        $model = $this->getMethodInstance();
        $orderIncrementId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
        $order = Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);

        if (!($order instanceof Mage_Sales_Model_Order)) {
            Mage::throwException($this->_getHelper()->__('Cannot retrieve order object'));
        }

        if (!$status = $model->getConfigData('order_status_payment_canceled')) {
            $status = $order->getStatus();
        }

        if ($status == Mage_Sales_Model_Order::STATE_HOLDED && $order->canHold()) {
            $order->hold();
            $order->addStatusHistoryComment($this->__('The order has been blocked by the customer.'));
            $order->save();
        } elseif ($status == Mage_Sales_Model_Order::STATE_CANCELED && $order->canCancel()) {
            $order->cancel();
            $order->addStatusHistoryComment($this->__('The order has been canceled by the customer.'));
            $order->save();
        }

        if (!$model->getConfigData('canceled_empty_cart')) {
            $this->_reorder($order);
        }

        if ($model->getConfigData('canceled_redirect_cart')) {
            $this->_redirect('checkout/cart/');
        } else {
            $this->_redirect('checkout/onepage/failure');
        }
    }

    public function notifyAction()
    {

        $model = $this->getMethodInstance();
        if (!$this->hasRequiredData()) {
            return false;
        }

        if (!$order = Mage::getModel('sales/order')->loadByIncrementId($this->getPostValue('order_id'))) {
            return false;
        }

        if ($order->getState() == Mage_Sales_Model_Order::STATE_HOLDED) {
            $order->unhold();
        }

        if (!$status = $model->getConfigData('order_status_payment_accepted')) {
            $status = $order->getStatus();
        }

        $message = $this->getSuccessfulPaymentMessage();
        if ($status == Mage_Sales_Model_Order::STATE_PROCESSING) {
            if ($model->getConfigData('invoice_create')) {
                $this->saveInvoice($order);
            }
            $order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, $status, $message);
        } else if ($status == Mage_Sales_Model_Order::STATE_COMPLETE) {
            $this->saveInvoice($order);
            if ($order->canShip()) {
                $itemQty = $order->getItemsCollection()->count();
                $shipment = Mage::getModel('sales/service_order', $order)->prepareShipment($itemQty);
                $shipment->create($order->getIncrementId());
            }
        } else {
            if ($model->getConfigData('invoice_create')) {
                $this->saveInvoice($order);
            }
            $order->addStatusToHistory($status, $message, true);
        }

        $order->save();
        if (!$order->getEmailSent()) {
            $order->sendNewOrderEmail();
        }

        return true;

    }

    public function changemethodAction()
    {
        $method = $this->getRequest()->getParam('method');
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $session = Mage::getSingleton('core/session');
        $currencyCode = $quote->getQuoteCurrencyCode();

        $payment = $quote->getPayment();
        $payment->setMethod($method);

        $this->getApiInstance()->sendAmplitudeRequest(array(
            'device_id' => $session->getSessionId(),
            'event_type' => 'Click  - Merchant checkout - Change payment method',
            'event_properties' => array(
                'merchantID' => $this->getMethodInstance()->getMerchantId(),
                'merchantSite' => Mage::app()->getStore()->getName(),
                'merchantURL' => Mage::app()->getStore()->getBaseUrl(),
                'moonaPaymentPosition' => $this->getMethodInstance()->getPosition(),
                'currency' => $currencyCode,
                'currencySymbol' => Mage::app()->getLocale()->currency($currencyCode)->getSymbol(),
                'orderTotalValue' => number_format($quote->getGrandTotal(),2, '.', ',' ),
                'paymentMethodID' => $payment->getMethod(),
                'paymentMethodLabel' => $payment->getMethodInstance()->getTitle(),
            )
        ));

        return true;
    }

    protected function _reorder($order)
    {
        $cart = Mage::getSingleton('checkout/cart');

        $items = $order->getItemsCollection();
        foreach ($items as $item) {
            try {
                $cart->addOrderItem($item);
            } catch (Mage_Core_Exception $e) {
                if (Mage::getSingleton('checkout/session')->getUseNotice(true)) {
                    Mage::getSingleton('checkout/session')->addNotice($e->getMessage());
                } else {
                    Mage::getSingleton('checkout/session')->addError($e->getMessage());
                }
            } catch (Exception $e) {
                Mage::getSingleton('checkout/session')->addException(
                    $e,
                    Mage::helper('checkout')->__('Cannot add the item to shopping cart.')
                );
            }
        }

        $cart->save();
    }

    protected function getSuccessfulPaymentMessage()
    {
        $msg = __('IPN ok | validated by Moona');
        $msg .= '<br />'.sprintf( __( 'API : %1$s | PLUGIN : %2$s | transaction : %3$s | customer : %4$s'),
                $this->getPostValue('api_version'), $this->getPostValue('plugin_version'), $this->getPostValue('transaction_id'), $this->getPostValue('customer'));
        $msg .= '<br />'.sprintf( __( 'Transfer group : %s'), $this->getPostValue('transfer_group'));

        return $msg;
    }

    protected function saveInvoice(Mage_Sales_Model_Order $order, $ship = false)
    {
        if ($order->canInvoice()) {
            $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
            $invoice->register()->capture();

            $transactionSave = Mage::getModel('core/resource_transaction')
                ->addObject($invoice)
                ->addObject($invoice->getOrder());

            if ($ship && $order->canShip()) {
                $shipment = Mage::getModel('sales/service_order', $order)->prepareShipment();
                $shipment->register();
                $transactionSave->addObject($shipment);
            }

            $transactionSave->save();
            return true;
        }

        return false;
    }

    protected function hasRequiredData()
    {
        return !empty($this->getPostValue('order_id'));
    }

    protected function getPostValue($key, $default = '')
    {
        $input = file_get_contents( 'php://input' );
        $post = json_decode($input, true);
        $data = $post['session'];

        return isset($data[$key]) ? $data[$key] : $default;
    }

    protected function _getIframeBlock()
    {
        $this->loadLayout('checkout');
        return $this->getLayout()
            ->createBlock('moona/iframe');
    }
}