<?php

class Moona_Moona_Model_Payment extends Mage_Payment_Model_Method_Abstract
{
    const PLUGIN_VERSION = 'M1_1.1.0';

    protected $_code = 'moona_payment';
    protected $_formBlockType = 'moona/form';

    protected $_isGateway = false;
    protected $_canAuthorize = true;
    protected $_canCapture = true;
    protected $_canCapturePartial = false;
    protected $_canRefund = false;
    protected $_canVoid = false;
    protected $_canUseInternal = true;
    protected $_canUseCheckout = true;
    protected $_canUseForMultishipping = false;
    protected $_order = null;

    /**
     * @return string
     */
    public function getTitle()
    {
        return 'Moona';
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        if (!$version = $this->getConfigData('version')) {
            $version = '1.0.0';
        }

        return $version;
    }

    /**
     * @return string
     */
    public function getVersionShort()
    {
        $version = explode('.',$this->getVersion());
        $versionShort = $version[0].'.'.$version[1];

        return $versionShort;
    }

    /**
     * @return string
     */
    public function getPluginVersion()
    {
        return self::PLUGIN_VERSION;
    }

    /**
     * @return bool
     */
    public function isStaging()
    {
        return ($this->getConfigData('environment') === 'test');
    }

    /**
     * @return string
     */
    public function getPublishableSecret()
    {
        $value = $this->getConfigData( $this->isStaging() ? 'test_secret_key' : 'live_secret_key' );
        return Mage::helper('core')->decrypt($value);
    }


    /**
     * @return bool
     */
    public function isActive()
    {
        return (bool) $this->getConfigData( 'active' );
    }

    /**
     * @param string $section
     * @return bool
     */
    public function isBannerAvailable($section)
    {
        return (bool) ($this->isActive() && $this->getConfigData($section));
    }

    /**
     * @return null|int
     */
    public function getPosition()
    {
        $position = $this->getConfigData('sort_order');
        return $this->isActive() && !empty($position) ?
            $position :
            -1
            ;
    }

    /**
     * @return null|string
     */
    public function getMerchantId()
    {
        return $this->isActive() ?
            $this->getConfigData('merchant_id') :
            ''
            ;
    }

    /**
     * @return string
     */
    public function getSuccessRedirectUrl()
    {
        return Mage::getUrl('checkout/onepage/success', array('_secure' => true));
    }

    /**
     * @return string
     */
    protected function getNotificationRedirectUrl()
    {
        return  Mage::getUrl('moona/payment/notify', array('_secure' => true));
    }

    /**
     * @return string
     */
    public function getBackRedirectUrl()
    {
        return Mage::getUrl('moona/payment/back',  array('_secure' => true));
    }

    /**
     *  @return	 string
     */
    public function getOrderPlaceRedirectUrl()
    {
        return Mage::getUrl('moona/payment/redirect');

    }

    /**
     * @param $name
     * @return object
     */
    public function createFormBlock($name)
    {
        $block = $this->getLayout()->createBlock('moona/form_payment', $name);
        $block->setMethod($this->_code);
        $block->setPayment($this->getPayment());

        return $block;
    }

    /**
     * @param $mode
     * @return     array
     */
    public function getStandardCheckoutFormFields($mode)
    {
        $orderIncrementId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
        $order = Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);

        if (!($order instanceof Mage_Sales_Model_Order)) {
            Mage::throwException($this->_getHelper()->__('Cannot retrieve order object'));
        }

        $address = $order->getBillingAddress();

        $fields = array(
            "plugin_version" => $this->getPluginVersion(),
            "api_version" => '1.0.0',
            "amount" => ($order->getBaseGrandTotal()*100),
            "currency" => $order->getBaseCurrencyCode(),
            "email" => $order->getCustomerEmail(),
            "firstname" => $address->getFirstname(),
            "lastname" => $address->getLastname(),
            "phone" => $address->getTelephone(),
            "address" => $this->getStreetFormatted($address->getStreet()),
            "postcode" => $address->getPostcode(),
            "city" => $address->getCity(),
            "country" => $address->getCountryId(),
            "return_url" => $this->getSuccessRedirectUrl(),
            "cancel_url" => $this->getBackRedirectUrl(),
            "notification_url" => $this->getNotificationRedirectUrl(),
            "order_id" => $order->getIncrementId(),
            'device_id' => $order->getSessionId(),
            'mode' => $mode
        );

        return $fields;
    }


    /**
     * @param array|string $street
     * @return string
     */
    private function getStreetFormatted($street)
    {
        if(is_array($street)) {
            $streetFormatted = implode(' ', $street);
        } else {
            $streetFormatted = $street;
        }

        return $streetFormatted;
    }
}