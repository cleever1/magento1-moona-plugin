<?php

class Moona_Moona_System_Config_Source_PaymentMethod
{
    const MOONA = 'moona';


    /**
     * @var array
     */
    protected $options;

    /**
     * {@inheritdoc}
     */
    public function toOptionArray()
    {
        if (!$this->options) {
            $options = [];
            $paymentMethods = $this->getOptions();

            foreach ($paymentMethods as $value => $label) {
                $options[] = [
                    'value' => $value,
                    'label' => $label,
                ];
            }

            $this->options = $options;
        }

        return $this->options;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            self::MOONA => Mage::helper('Moona Payment')
        ];
    }

    /**
     * @param string $method
     * @return string
     */
    public function getServiceName($method)
    {
        switch ($method) {
            default:
                $serviceName = 'Moona';
                break;
        }

        return $serviceName;
    }

    /**
     * @return array
     */
    public function getAvailablePaymentMethods()
    {
        return array_keys($this->getOptions());
    }

    /**
     * @param $methodCode
     * @return bool|mixed
     */
    public function getPaymentMethodTranslation($methodCode)
    {
        $options = $this->getOptions();

        return isset($options[$methodCode]) ? $options[$methodCode] : false;
    }
}
