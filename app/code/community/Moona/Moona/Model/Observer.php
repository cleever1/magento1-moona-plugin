<?php

class Moona_Moona_Model_Observer
{
    /**
     * @return Moona_Moona_Model_Api
     */
    public function getApiInstance()
    {
        return Mage::getSingleton('moona/api');
    }

    /**
     * @return Moona_Moona_Model_Payment
     */
    public function getMethodInstance()
    {
        return Mage::getSingleton('moona/payment');
    }

    public function predispatchCheckout(Varien_Event_Observer $observer)
    {
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $session = Mage::getSingleton('core/session');
        $currencyCode = $quote->getQuoteCurrencyCode();

        if(!is_null($quote->getId())) {
            $this->getApiInstance()->sendAmplitudeRequest(array(
                'device_id' => $session->getSessionId(),
                'event_type' => 'View - Merchant checkout',
                'event_properties' => array(
                    'merchantID' => $this->getMethodInstance()->getMerchantId(),
                    'merchantSite' => Mage::app()->getStore()->getName(),
                    'merchantURL' => Mage::app()->getStore()->getBaseUrl(),
                    'moonaPaymentPosition' => $this->getMethodInstance()->getPosition(),
                    'currency' => $currencyCode,
                    'currencySymbol' => Mage::app()->getLocale()->currency($currencyCode)->getSymbol(),
                    'orderTotalValue' => number_format($quote->getGrandTotal(),2, '.', ',' )
                )
            ));
        }
    }

    public function orderPlaceEnd(Varien_Event_Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        $payment = $order->getPayment();
        $session = Mage::getSingleton('core/session');
        $currencyCode = $order->getQuoteCurrencyCode();

        $this->getApiInstance()->sendAmplitudeRequest(array(
            'device_id' => $session->getSessionId(),
            'event_type' => 'Click - Merchant checkout',
            'event_properties' => array(
                'merchantID' => $this->getMethodInstance()->getMerchantId(),
                'merchantSite' => Mage::app()->getStore()->getName(),
                'merchantURL' => Mage::app()->getStore()->getBaseUrl(),
                'moonaPaymentPosition' => $this->getMethodInstance()->getPosition(),
                'orderID' => $order->getIncrementId(),
                'currency' => $currencyCode,
                'currencySymbol' => Mage::app()->getLocale()->currency($currencyCode)->getSymbol(),
                'orderTotalValue' => number_format($order->getGrandTotal(),2, '.', ',' ),
                'paymentMethodID' => $payment->getMethod(),
                'paymentMethodLabel' => $payment->getMethodInstance()->getTitle()
            )
        ));

        $this->getApiInstance()->sendAmplitudeRequest(array(
            'device_id' => $session->getSessionId(),
            'event_type' => 'Order - Status',
            'event_properties' => array(
                'merchantID' => $this->getMethodInstance()->getMerchantId(),
                'merchantSite' => Mage::app()->getStore()->getName(),
                'merchantURL' => Mage::app()->getStore()->getBaseUrl(),
                'orderID' => $order->getIncrementId(),
                'currency' => $currencyCode,
                'currencySymbol' => Mage::app()->getLocale()->currency($currencyCode)->getSymbol(),
                'orderTotalValue' => number_format($order->getGrandTotal(),2, '.', ',' ),
                'paymentMethodID' => $payment->getMethod(),
                'paymentMethodLabel' => $payment->getMethodInstance()->getTitle(),
                'orderStatus' => $order->getStatus()
            )
        ));
    }

    public function orderPlaceAfter(Varien_Event_Observer $observer)
    {
        $orderIds = $observer->getEvent()->getOrderIds();
        $session = Mage::getSingleton('core/session');

        foreach($orderIds as $orderid) {

            $order = Mage::getModel('sales/order')->load($orderid);
            $payment = $order->getPayment();
            $currencyCode = $order->getQuoteCurrencyCode();

            if (!is_null($order->getId()) && !is_null($payment->getId())) {

                $this->getApiInstance()->sendAmplitudeRequest(array(
                    'device_id' => $session->getSessionId(),
                    'event_type' => 'View - Order confirmation',
                    'event_properties' => array(
                        'merchantID' => $this->getMethodInstance()->getMerchantId(),
                        'merchantSite' => Mage::app()->getStore()->getName(),
                        'merchantURL' => Mage::app()->getStore()->getBaseUrl(),
                        'orderID' => $order->getIncrementId(),
                        'currency' => $currencyCode,
                        'currencySymbol' => Mage::app()->getLocale()->currency($currencyCode)->getSymbol(),
                        'orderTotalValue' => number_format($order->getGrandTotal(),2, '.', ',' ),
                        'paymentMethodID' => $payment->getMethod(),
                        'paymentMethodLabel' => $payment->getMethodInstance()->getTitle(),
                        'orderStatus' => $order->getStatus()
                    )
                ));
            }
        }
    }

    public function injectSession(Varien_Event_Observer $observer)
    {
        $session = Mage::getSingleton('core/session');
        $order = $observer->getEvent()->getOrder();

        $order->setSessionId($session->getSessionId());

        return $this;
    }

    public function tracking(Varien_Event_Observer $observer)
    {

        if(isset($_POST['groups']['moona_payment']['fields']['active']['value'])) {

            $value = (bool) $_POST['groups']['moona_payment']['fields']['active']['value'];
            $api = $this->getApiInstance();

            $api->sendPluginStatus(array(
                'status' => ($value ? 'enabled' : 'disabled'),
                'options' => json_encode($_POST['groups']['moona_payment'])
            ));

            if($value == 'enabled') {
                $merchantId = $api->getMerchantId();
                Mage::getModel('core/config')->saveConfig('payment/moona_payment/merchant_id', $merchantId, 'default', 0);
            }

        }
    }

    public function orderSaveAfter(Varien_Event_Observer $observer)
    {

        $order = $observer->getEvent()->getOrder();
        $payment = $order->getPayment();
        $currencyCode = $order->getQuoteCurrencyCode();

        if(!is_null($order->getId()) && !is_null($payment->getId())) {
            $this->getApiInstance()->sendAmplitudeRequest(array(
                'device_id' => $order->getSessionId(),
                'event_type' => 'Order - Status',
                'event_properties' => array(
                    'merchantID' => $this->getMethodInstance()->getMerchantId(),
                    'merchantSite' => Mage::app()->getStore()->getName(),
                    'merchantURL' => Mage::app()->getStore()->getBaseUrl(),
                    'orderID' => $order->getIncrementId(),
                    'currency' => $currencyCode,
                    'currencySymbol' => Mage::app()->getLocale()->currency($currencyCode)->getSymbol(),
                    'orderTotalValue' => number_format($order->getGrandTotal(),2, '.', ',' ),
                    'paymentMethodID' => $payment->getMethod(),
                    'paymentMethodLabel' => $payment->getMethodInstance()->getTitle(),
                    'orderStatus' => $order->getStatus()
                )
            ));
        }
    }
}