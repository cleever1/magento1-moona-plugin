<?php


class Moona_Moona_Model_System_Config_Source_Order_Status_Refused extends Moona_Moona_Model_System_Config_Source_Order_Status
{
    protected $_stateStatuses = array(
        Mage_Sales_Model_Order::STATE_HOLDED,
        Mage_Sales_Model_Order::STATE_CANCELED
    );
}
