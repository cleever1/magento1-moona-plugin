<?php

class Moona_Moona_Block_Iframe extends Mage_Payment_Block_Form
{

    /**
     *
     * @return Moona_Moona_Model_Api
     */
    public function getApiInstance()
    {
        return Mage::getSingleton('moona/api');
    }

    /**
     *
     * @return Moona_Moona_Model_Payment
     */
    public function getMethodInstance()
    {
        return Mage::getSingleton('moona/payment');
    }

    protected function _toHtml()
    {
        $this->setTemplate('moona/iframe.phtml');
        return parent::_toHtml();
    }

    public function getUrlIframe()
    {
        $data = $this->getApiInstance()->getRedirectUrl(
            $this->getMethodInstance()->getStandardCheckoutFormFields('iframe')
        );
        $response = json_decode($data['response'], true);

        if($data['error']) {
            Mage::throwException($this->_getHelper()->__('An error has occured. '. $response['response']));
        }

        return $response['url'];
    }

    public function getSuccessRedirectUrl()
    {
        return $this->getMethodInstance()->getSuccessRedirectUrl();
    }

    public function getBackRedirectUrl()
    {
        return $this->getMethodInstance()->getBackRedirectUrl();
    }
}