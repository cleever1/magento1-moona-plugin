<?php

class Moona_Moona_Block_Total extends Mage_Payment_Block_Form
{

    /**
     *
     * @return Moona_Moona_Model_Api
     */
    public function getApiInstance()
    {
        return Mage::getSingleton('moona/api');
    }

    protected function _toHtml()
    {
        return parent::_toHtml();
    }

    public function getTotal()
    {
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $method = $quote->getPayment()->getMethodInstance()->getCode();
        $currencyCode = $quote->getQuoteCurrencyCode();

        $arrayData = array(
            'price' => $quote->getGrandTotal()*100,
            'lang' => Mage::app()->getLocale()->getLocaleCode(),
            'currency_symbol' => Mage::app()->getLocale()->currency($currencyCode)->getSymbol(),
            'currency_code' => $currencyCode,
            'payment_method' => $method == 'moona_payment' ? 'moona' : $method,
            'source' => 'checkout'
        );

        $data = $this->getApiInstance()->getTotal($arrayData);
        $response = json_decode($data['response'], true);

        if($data['error']) {
            return array();
        }

        return $response;
    }

}