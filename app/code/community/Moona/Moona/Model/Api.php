<?php


class Moona_Moona_Model_Api
{
    const ENDPOINT_TEST = 'https://staging.api.moona.com';
    const ENDPOINT_LIVE = 'https://api.moona.com';

    const ENDPOINT_AMPLITUDE = 'https://api2.amplitude.com/2/httpapi';
    const KEY_AMPLITUDE_TEST = 'a19d8d2c919ec80110ac6d2353253c9e';
    const KEY_AMPLITUDE_LIVE = 'fceaa7f5144bfd64c3281bbca7cf0bac';

    protected $_body = '';
    protected $_status;

    /**
     * @return Moona_Moona_Model_Payment
     */
    public function getMethodInstance()
    {
        return Mage::getSingleton('moona/payment');
    }

    /**
     * @param string $url
     * @param array $data
     */
    private function initRequest( $url = '', array $data = array())
    {
        $model = $this->getMethodInstance();
        $data['ecommerce_solution'] = 'Magento1';
        $data['ecommerce_version'] = $model->getVersion();
        $data['plugin_version'] = $model->getPluginVersion();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, ($model->isStaging() ? self::ENDPOINT_TEST : self::ENDPOINT_LIVE).$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type:application/json; charset=utf-8',
            'x-api-key:'.$model->getPublishableSecret()
        ));
        $this->_body = curl_exec($ch);
        $this->_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
    }

    /**
     * @param array $data
     */
    private function initRequestAmplitude( array $data = array())
    {
        $model = $this->getMethodInstance();
        if(!$model->isActive()) return ;

        $data['event_properties']['ecommerceSolution'] = 'Magento1';
        $data['event_properties']['ecommerceVersion'] = $model->getVersion();
        $data['event_properties']['ecommerceShortVersion '] = $model->getVersionShort();
        $data['event_properties']['pluginVersion'] = $model->getPluginVersion();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::ENDPOINT_AMPLITUDE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array(
            'api_key' => ($model->isStaging() ? self::KEY_AMPLITUDE_TEST : self::KEY_AMPLITUDE_LIVE),
            'events' => array(
                $data
            )
        )));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type:application/json; charset=utf-8',
            'Accept:*/*'
        ));
        $this->_body = curl_exec($ch);
        $this->_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
    }


    /**
     * @param array $data
     * @return array
     */
    public function getRedirectUrl(array $data)
    {
        $this->initRequest('', $data);

        return array(
            'error' => $this->_status != 200,
            'response' => $this->_body
        );
    }

    /**
     * @param array $data
     * @return array
     */
    public function getBanners(array $data)
    {
        $this->initRequest('/ecommerce/banners', $data);
        $status = $this->_status;
        $body = $this->_body;

        $banners = array();
        if($status == 200) {
            $banners = json_decode( $body, true );
            $banners = isset($banners['banners']) ? $banners['banners'] : array();
        }

        return $banners;
    }

    /**
     * @param array $data
     * @return array
     */
    public function getTotal(array $data)
    {
        if(!$this->getMethodInstance()->isActive()) {
            return array(
                'error' => true
            );
        }
        $this->initRequest('/ecommerce/display_discount', $data);

        return array(
            'error' => $this->_status != 200,
            'response' => $this->_body
        );
    }

    /**
     * @param array $data
     * @return array
     */
    public function getCheckoutBanner(array $data)
    {
        $this->initRequest('/ecommerce/checkout', $data);

        return array(
            'error' => $this->_status != 200,
            'response' => $this->_body
        );
    }

    /**
     * @return string
     */
    public function getMerchantId()
    {
        $this->initRequest('/ecommerce/merchant');
        $status = $this->_status;
        $body = $this->_body;

        $merchantId = array();
        if($status == 200) {
            $merchantId = json_decode( $body, true );
            $merchantId = isset($merchantId['merchantId']) ? $merchantId['merchantId'] : array();
        }

        return $merchantId;
    }

    /**
     * @param array $data
     */
    public function sendAmplitudeRequest(array $data)
    {
        $this->initRequestAmplitude($data);
    }

    /**
     * @param array $data
     */
    public function sendPluginStatus(array $data)
    {
        $this->initRequest('/ecommerce/status', $data);
    }

    /**
     * @return array
     */
    public function getDisplayMode()
    {
        $this->initRequest('/ecommerce/display_mode');

        return array(
            'error' => $this->_status != 200,
            'response' => $this->_body
        );
    }
}