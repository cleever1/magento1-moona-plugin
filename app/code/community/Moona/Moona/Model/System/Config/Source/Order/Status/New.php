<?php

class Moona_Moona_Model_System_Config_Source_Order_Status_New extends Moona_Moona_Model_System_Config_Source_Order_Status
{
    protected $_stateStatuses = array(
        Mage_Sales_Model_Order::STATE_NEW,
        Mage_Sales_Model_Order::STATE_PROCESSING
    );

}
