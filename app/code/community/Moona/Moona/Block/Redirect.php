<?php

class Moona_Moona_Block_Redirect extends Mage_Payment_Block_Form
{

    /**
     *
     * @return Moona_Moona_Model_Api
     */
    public function getApiInstance()
    {
        return Mage::getSingleton('moona/api');
    }

    /**
     *
     * @return Moona_Moona_Model_Payment
     */
    public function getMethodInstance()
    {
        return Mage::getSingleton('moona/payment');
    }

    protected function _toHtml()
    {

        $form = new Varien_Data_Form();
        $data = $this->getApiInstance()->getRedirectUrl(
            $this->getMethodInstance()->getStandardCheckoutFormFields('redirection')
        );
        $response = json_decode($data['response'], true);

        if($data['error']) {
            Mage::throwException($this->_getHelper()->__('An error has occured. '. $response['response']));
        }

        $form->setAction($response['url'])
            ->setId('moona_checkout')
            ->setName('moona_checkout')
            ->setMethod('POST')
            ->setUseContainer(true);

        $submitButton = new Varien_Data_Form_Element_Submit(array(
            'value'    => $this->__('Click here if you are not redirected within 10 seconds...'),
        ));

        $form->addElement($submitButton);
        $html = '<html><body>';
        $html.= $this->__('You will be redirected to the Moona website in a few seconds.');
        $html.= $form->toHtml();
        $html.= '<script type="text/javascript">document.getElementById("moona_checkout").submit();</script>';
        $html.= '</body></html>';

        return $html;
    }

}