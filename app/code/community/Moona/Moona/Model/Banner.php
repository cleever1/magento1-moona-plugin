<?php

class Moona_Moona_Model_Banner
{
    /**
     * @var array
     */
    protected $_banners;

    /**
     * @return Moona_Moona_Model_Payment
     */
    public function getMethodInstance()
    {
        return Mage::getSingleton('moona/payment');
    }

    /**
     * @return Moona_Moona_Model_Api
     */
    public function getApiInstance()
    {
        return Mage::getSingleton('moona/api');
    }

    public function __construct() {
        $this->_banners = $this->getApiInstance()->getBanners(array(
            'lang' => Mage::app()->getLocale()->getLocaleCode()
        ));
    }

    /**
     * @param string $section
     * @return bool
     */
    public function isBannerAvailable($section)
    {
        return $this->getMethodInstance()->isBannerAvailable($section);
    }

    /**
     * @param string $section
     * @return string
     */
    public function getHtmlBanner($section)
    {
        if(isset($this->_banners[$section])) {
            return $this->_banners[$section];
        }

        return '';
    }
}