Moona payment solution unlocks £5 discounts for your customers on each order, and at no cost to you!
# Description

Moona payment solution unlocks £5 discounts for your customers on each order, and at no cost to you!

When you install Moona on your online store you get 2 main things:

- **Increase conversion on your website** with a simple and 100% funded discount program by Moona, that boosts your sales
- **Increase your margin** with a 100% free secure payment platform accepting Visa, Mastercard and American Express cards. No hidden fees, it is all for FREE.

Our model is based on a yearly membership paid by shoppers, allowing them to receive unlimited £5 discounts on each order - at no cost to you. We cover the discounts, and payment transactions are secured and **100% free with no hidden fees.**

Moona is available now, and can be integrated directly into your Magento 2 online store in a few clicks.

## Why will you love Moona?

1. **Higher conversion**: With unlimited £5 upfront discounts, more shoppers complete their orders and fewer abandon their cart at checkout.
2. **Repeat business:** When customers know they can pay in 1 click with Moona and get instant discounts every time, they’ll come back to shop more often.
3. **Bigger carts:** Getting a discount on every order means customers are willing to add more to their carts, increasing your average order value.
4. **Zero payment fees:** Yes, you read that right. £0 set-up fee, £0 per transaction and 0% on volume.
5. **Secure payment:** Because we use Stripe technology, payments are made safe for your customers and you stay protected against fraud.
6. **Easy integration:** Getting started with Moona is easy for all Magento 2 stores. More e-commerce platforms to come soon.
7. **More traffic:** Shoppers are actively looking for retailers where they can get instant Moona discounts, boosting exposure and traffic to our network of partners.
8. **£5 discount on each order:** We cover the discounts we give to your customers on each order. It won’t cost you a thing, as our model is based on paid memberships for shoppers.

## Why will your shoppers love Moona? 

1. **Instant discounts:** Your customers get a £5 discount, courtesy of Moona, on every order. We’ll automatically adjust their order total at the payment step.
2. **Faster checkout:** With Moona's simplified payment process, shoppers can checkout seamlessly in 1 click.
3. **Secure payment:** Stripe-powered technology means credit card details stay safe, always.
4. **Better shopping options:** Moona’s network of retail partners helps customers discover new shops that offer them instant discounts and a seamless payment process.

And much more to come! Stay tuned.


# Installation

- Backup your store database and web directory
- Login to Magento backend
- Go to System > Tools > Compilations, if Compiler Status is Enabled, disable the compilation
- Uncompress the archive file, then copy directories "app", "js" and "lib" in your Magento root directory
- Go to System > Cache Management. Click button Flush Magento Cache
- Logout from Magento backend and login again

## How to completely uninstall the extension ?
After having uninstalled the extension in Magento Connect Manager, some references remain in the database.  
To solve this problem, you must execute the following MySQL query:

    DELETE FROM `core_config_data` WHERE `path` LIKE 'payment/moona_payment/%'