<?php

class Moona_Moona_Block_Banner extends Mage_Payment_Block_Form
{

    private $_section;

    /**
     *
     * @return Moona_Moona_Model_Banner
     */
    public function getBannerInstance()
    {
        return Mage::getSingleton('moona/banner');
    }

    protected function _toHtml()
    {
        if (!$this->getBannerInstance()->isBannerAvailable($this->_section)) {
            return '';
        }

        $html = $this->getBannerInstance()->getHtmlBanner($this->_section);
        if($html != '') {
            $this->setData('html', $html);
        }

        return parent::_toHtml();
    }

    public function setSection($section) {
        $this->_section = $section;
    }

    public function getSection()
    {
        return $this->_section;
    }
}