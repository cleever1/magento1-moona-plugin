<?php

class Moona_Moona_Model_System_Config_Source_Environment
{

    public function toOptionArray()
    {
        return array(
            array(
                'value' => 'test',
                'label' => Mage::helper('adminhtml')->__('Test'),
            ),
            array(
                'value' => 'production',
                'label' => Mage::helper('adminhtml')->__('Production'),
            )
        );
    }
}
